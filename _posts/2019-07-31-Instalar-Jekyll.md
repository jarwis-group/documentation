---
title: "Instalar Jekyll" 
published: true 
permalink: Instalar-Jekyll.html 
summary: "Instalar Jekyll" 
tags: [news, getting_started] 
---
sudo apt-get install ruby-full build-essential zlib1g-dev

echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc

gem install jekyll bundler

{% include links.html %}