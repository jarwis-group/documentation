import json
import requests
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

PRIVATE_TOKEN = config['PRODUCTION']['PRIVATE_TOKEN']
id_repo = 0

def createGraphicPie(abiertos,cerrados):
	content = "<script type='text/javascript' src='https://cdn.rawgit.com/mbostock/protovis/v3.3.1/protovis.js'></script><center>\n<script src='https://d3js.org/d3.v3.min.js'></script>\n<script type='text/javascript+protovis'>\nvar data = [\n{dept:'Accounting', values: [\n{name:'abiertos', value:"+str(abiertos)+"},\n{name:'cerrados', value:"+str(cerrados)+"}\n]},\n];\n\nvar w = 280,\nh = w,\nr = w / 2;\n\ndata.forEach(function(d) d.scale = pv.Scale.linear(0,pv.sum(d.values, function(d) d.value)).range(0, 2 * Math.PI));\n\nvar vis = new pv.Panel().data(data).width(w).height(h).margin(10);\n\nvis.add(pv.Wedge).data(function(d) d.values).bottom(w / 2).left(w / 2).innerRadius(r - 40).outerRadius(r).angle(function(d, p) p.scale(d.value)).anchor('center').add(pv.Label).text(function(d) d.name);\n\nvis.render();\n</script>\n</center>\n"
	return content

def graphicBar(abiertos,cerrados):
	content = "<script src='https://d3js.org/d3.v3.min.js'></script><style>\n\n.chart rect {\nfill: steelblue;\n}\n\n.chart text {\nfill: white\nfont: 10px sans-serif;\ntext-anchor: end;\n}\n\n</style>\n<svg class='chart'></svg>\n\n<script>\n\nvar data = ["+str(abiertos)+","+str(cerrados)+"];\n\nvar width = 420,barHeight = 20;\n\nvar x = d3.scale.linear().domain([0, d3.max(data)]).range([0, width]);\n\nvar chart = d3.select('.chart').attr('width', width).attr('height', barHeight * data.length);\n\nvar bar = chart.selectAll('g').data(data).enter().append('g').attr('transform', function(d, i) { return 'translate(0,' + i * barHeight + ')'; });\n\nbar.append('rect').attr('width', x).attr('height', barHeight - 1);\n\nbar.append('text').attr('x', function(d) { return x(d) - 3; }).attr('y', barHeight / 2).attr('dy', '.35em').text(function(d) {return d; });\n\n</script>"
	return content

def createGraphicBar(etiquetas):
	content = "<style>\n\n.chart rect {\nfill: steelblue;\n}\n\n.chart text {\nfill: white\nfont: 10px sans-serif;\ntext-anchor: end;\n}\n\n</style>\n<svg class='chart'></svg>\n\n<script>\n\nvar data = ["

	for etiqueta in etiquetas:
		content = content + "'"+etiqueta +"',"

	content = content + "];\n\nvar width = 420,barHeight = 20;\n\nvar x = d3.scale.linear().domain([0, d3.max(data)]).range([0, width]);\n\nvar chart = d3.select('.chart').attr('width', width).attr('height', barHeight * data.length);\n\nvar bar = chart.selectAll('g').data(data).enter().append('g').attr('transform', function(d, i) { return 'translate(0,' + i * barHeight + ')'; });\n\nbar.append('rect').attr('width', x).attr('height', barHeight - 1);\n\nbar.append('text').attr('x', function(d) { return x(d) - 3; }).attr('y', barHeight / 2).attr('dy', '.35em').text(function(d) {return d; });\n\n</script>"
	return content

def getTitulo(id):
    print("id"+str(id))
    print(PRIVATE_TOKEN)
    r = requests.get('https://gitlab.com/api/v4/projects/'+id+'/', params = {'PRIVATE-TOKEN':PRIVATE_TOKEN})
    if r.status_code == 200:
        return r.text
    else:
        print("ERROR: "+ str(r.status_code))
        return r.status_code

def getIssues(id):
    
    r = requests.get('https://gitlab.com/api/v4/projects/'+id+'/issues', params = {'PRIVATE-TOKEN':PRIVATE_TOKEN})
   
    if r.status_code == 200:
        return r.text
    else:
        return r.status_code 


def getEtiquetas(id):
    
	r = requests.get('https://gitlab.com/api/v4/projects/'+id+'/labels', params = {'PRIVATE-TOKEN':PRIVATE_TOKEN})
   
	if r.status_code == 200:
		return r.text
	else:
		return r.status_code 

def getCabecera(id):
	titulo = json.loads(getTitulo(id))
	cabecera ="---\ntitle:Documentación de "+titulo['name']+"\nkeywords: sample homepage\ntags: [getting_started]\nsidebar: mydoc_sidebar\npermalink: index.html\nsummary: Este es mi resumen.\n---\n\n"
	return cabecera
#print(titulo['name'])
#print(cabecera)

def labelIssues(id):
	issues = json.loads(getIssues(id))
	etiquetas = set()
	
	for issue in issues:
		etiqueta = issue['labels']
		if etiqueta:				
			etiquetas.add(etiqueta[0])

	return etiquetas

def statusIssues(id):
	issues = json.loads(getIssues(id))
	abiertos = 0
	cerrados = 0
	for issue in issues:
		if(issue['state'] == 'opened'):
			abiertos = abiertos +1
		else:
			cerrados = cerrados + 1
		
	status = dict(abiertos=abiertos, cerrados=cerrados)
	return status



def generateIndex(id):
	
	estadoIssues = statusIssues(id)
	abiertos = estadoIssues.get('abiertos')
	cerrados = estadoIssues.get('cerrados')

	print("Etiquetas: "+str(labelIssues(id)))
	print("Issues abiertos: "+str(abiertos))
	print("Issues cerrados: "+str(cerrados))
	print("\n")

	web = getCabecera(id)+"\n"+createGraphicPie(abiertos,cerrados)+"\n"+graphicBar(abiertos,cerrados)+"\n{% include links.html %}"
	fichero = open('index.md','w')
	fichero.write(web)
	fichero.close()
	
	return web


