---
title:Documentación de webDocumentacion
keywords: sample homepage
tags: [getting_started]
sidebar: mydoc_sidebar
permalink: index.html
summary: Este es mi resumen.
---


<script type='text/javascript' src='https://cdn.rawgit.com/mbostock/protovis/v3.3.1/protovis.js'></script><center>
<script src='https://d3js.org/d3.v3.min.js'></script>
<script type='text/javascript+protovis'>
var data = [
{dept:'Accounting', values: [
{name:'abiertos', value:4},
{name:'cerrados', value:1}
]},
];

var w = 280,
h = w,
r = w / 2;

data.forEach(function(d) d.scale = pv.Scale.linear(0,pv.sum(d.values, function(d) d.value)).range(0, 2 * Math.PI));

var vis = new pv.Panel().data(data).width(w).height(h).margin(10);

vis.add(pv.Wedge).data(function(d) d.values).bottom(w / 2).left(w / 2).innerRadius(r - 40).outerRadius(r).angle(function(d, p) p.scale(d.value)).anchor('center').add(pv.Label).text(function(d) d.name);

vis.render();
</script>
</center>

<script src='https://d3js.org/d3.v3.min.js'></script><style>

.chart rect {
fill: steelblue;
}

.chart text {
fill: white
font: 10px sans-serif;
text-anchor: end;
}

</style>
<svg class='chart'></svg>

<script>

var data = [4,1];

var width = 420,barHeight = 20;

var x = d3.scale.linear().domain([0, d3.max(data)]).range([0, width]);

var chart = d3.select('.chart').attr('width', width).attr('height', barHeight * data.length);

var bar = chart.selectAll('g').data(data).enter().append('g').attr('transform', function(d, i) { return 'translate(0,' + i * barHeight + ')'; });

bar.append('rect').attr('width', x).attr('height', barHeight - 1);

bar.append('text').attr('x', function(d) { return x(d) - 3; }).attr('y', barHeight / 2).attr('dy', '.35em').text(function(d) {return d; });

</script>
{% include links.html %}