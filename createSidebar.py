# This is your sidebar TOC. The sidebar code loops through sections here and provides the appropriate formatting.

def createSidebar(entradas,slug):
  i=0
  items = ""
  sidebar = "entries:\n- title: Sidebar\n  levels: one\n  folders:\n  - title: Entradas\n    output: web\n    folderitems:\n    - title: Indice\n      url: /news.html\n      output: web\n"
  
  for entrada in entradas:
      items = items+"    - title: "+entrada+"\n      url: /"+slug[i]+".html\n      output: web\n"
      i=i+1  
  sidebar = sidebar+items
  print(sidebar)
  return sidebar