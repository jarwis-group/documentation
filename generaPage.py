import json
import datetime
from io import open
import subprocess
import requests
import createSidebar
import generateIndex
import configparser
import sys

id = str(sys.argv[1])

config = configparser.ConfigParser()
config.read('config.ini')

PRIVATE_TOKEN = config['PRODUCTION']['PRIVATE_TOKEN']
print(PRIVATE_TOKEN)

titulos = list()
slug = list()

def pageContent(slug,id):
    
    r = requests.get('https://gitlab.com/api/v4/projects/'+id+'/wikis/'+slug, params = {'PRIVATE-TOKEN':PRIVATE_TOKEN})
    
    if r.status_code == 200:
        return r.text
    else:
        return r.status_code    
    

with open('paginas.json') as file:
    data = json.load(file)

    num = ('0','1','2','3','4','5','6','7','8','9')

    for pagina in data['paginas']:
        print('Titulo:', pagina['title'])       
        print('Slug:', pagina['slug'])
        print('')
        titulo = str(pagina['title'])
        
        titulos.append(titulo)
        slug.append(pagina['slug'])

        #recuperamos el contenido de la pagina
        pageJSON = json.loads(pageContent(pagina['slug'],id))

        #creamos el contenido de la web        
        content = '---\ntitle: "'+pagina['title'] +'" \npublished: true \npermalink: '+pagina['slug']+'.html \nsummary: "'+pagina['title']+'" \ntags: [news, getting_started] \n---\n'+pageJSON['content']+'\n{% include links.html %}'     	
        print(content)
        
        #El fichero debe tener el formato  YYYY-mm-dd-nombre.md
        dt = datetime.datetime.now()
        fecha = dt.strftime('%Y-%m-%d')
        nombreFichero = fecha+"-"+pagina['slug']+".md"
        fichero = open('_posts/'+nombreFichero,'w')
        
        
        fichero.write(content)
        fichero.close()
        
print("Titulos: " + str(titulos))
print("Slug: " + str(titulos))

print(generateIndex.generateIndex(id))

fichero = open('_data/sidebars/home_sidebar.yml','w')
fichero.write(createSidebar.createSidebar(titulos,slug))
fichero.close()

#comiteamos y subimos
subprocess.call(["git","add","."])
subprocess.call(["git","commit","-m","update"])
subprocess.call(["git","push"])




