import subprocess
from os import listdir
from os.path import isfile, join
import os
from git import Repo
from shutil import rmtree
import sys

ruta = str(sys.argv[1])
url = list()


#Obtenemos el directorio a partir de la ruta del repositorio
def getDirectorio(ruta):
	directorio = os.path.basename(ruta)
	directorio = directorio[0:(len(directorio)-4)]
	return directorio

#listamos todos los ficheros del directorio
def ls(directorio):
    return [arch for arch in listdir(directorio) if isfile(join(directorio, arch))]

#extraemos los nombers de los ficheros listados, sin extensión ni guiones
def extraerNombres(ficheros):
	paginas = list ()	
	ficheros.sort()
	for fichero in ficheros:
		nombre = fichero[0:(len(fichero)-3)]
		url.append(nombre)
		nombre = nombre.replace("-", " ")
		paginas.append(nombre)
	paginas.remove("home")
	url.remove("home")
	return paginas

#Generamos el contenido de la pagina de home
def makeContenido(ficheros,url):
	#-- contenido
	contenido = "<h1> INDEX </h1>"
	i = 0
	for fichero in ficheros:
		contenido = contenido + "\n*  [" + fichero+ "]("+url[i]+")<br>" 
		i = i + 1

	return contenido

#Guardamos el contenido en el fichro home.md que dibuja la portada
def guardarContenido(directorio, contenido):
	fichero = open(directorio+'/home.md','w')
	fichero.write(contenido)
	fichero.close()

#Hacemos push al repo
def git_push():
    try:
        repo = Repo(directorio)
        repo.git.add(update=True)
        repo.index.commit("Actualización del indice")
        origin = repo.remote(name='origin')
        origin.push()
    except:
        print('Some error occured while pushing the code')  


subprocess.call(["git","clone",ruta])#Clonamos el repositorio del Wiki
directorio = getDirectorio(ruta)
ficheros = extraerNombres(ls(directorio))
contenido = makeContenido(ficheros,url)
guardarContenido(directorio,contenido)

print(ficheros)
print(contenido)

git_push()
rmtree(directorio)#Una vez finalizado, borramos el directorio local del repo